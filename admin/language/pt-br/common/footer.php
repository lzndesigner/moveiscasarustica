<?php
// Text
$_['text_footer']  = 'Modificações por <a href="https://jpwtechdigital.com.br">JPW Tech Digital</a> <br> <a href="http://www.opencart.com">OpenCart</a> &copy; 2009-' . date('Y') . ' Todos os direitos reservados.';
$_['text_version'] = 'Versão %s';