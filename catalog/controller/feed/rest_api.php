<?php
error_reporting(0);

/**
 * © Copyright 2014-2020 Codemarket - Todos os direitos reservados.
 * Class ControllerFeedRestApi
 */
class ControllerFeedRestApi extends Controller
{
    private $debugIt = false;
    private $secretKey = 'bling123'; // Secret Key
    private $conf = '';
    private $log;

    /**
     * ControllerFeedRestApi constructor.
     *
     * @param $registry
     *
     * @throws \Exception
     */
    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->load->model('module/code_bling');
        $this->load->model('module/code_bling_native_order');
        $this->load->model('module/code_bling_native_product');

        $this->load->model('catalog/product');

        $this->load->model('module/codemarket_module');
        $conf = $this->model_module_codemarket_module->getModulo('548');

        if (
            empty($conf) || empty($conf->code_habilitar) || (int) $conf->code_habilitar === 2 ||
            empty($conf->code_token) || empty($conf->code_chave)
        ) {
            exit('Desativado ou sem configuração');
        }

        $this->conf = $conf;
        $this->secretKey = $conf->code_chave;
        $this->log = new log('CodeBlingRestAPI.log');
    }

    /*
    * Get products
    */
    public function products()
    {
        $this->checkPlugin();
        $this->log->write('products() - Rodando');
        $products = $this->model_module_code_bling_native_product->getAllProduct();

        if (count($products)) {
            foreach ($products as $product) {
                $variation = $this->model_module_code_bling_native_product->getVariation($product);

                if (empty($product['product_id'])) {
                    continue;
                }

                $codeProduct = $this->model_module_code_bling->getProduct($product['product_id']);

                if (empty($codeProduct['product_id'])) {
                    continue;
                }

                $productSKU = '';
                if (!empty($codeProduct['sku'])) {
                    $productSKU = $codeProduct['sku'];
                }

                if (!empty($this->conf->code_produto_ncm) && !empty($codeProduct[$this->conf->code_produto_ncm])) {
                    $productNCM = $codeProduct[$this->conf->code_produto_ncm];
                } else {
                    $productNCM = '';
                }

                if (!empty($this->conf->code_produto_cest) && !empty($codeProduct[$this->conf->code_produto_cest])) {
                    $productCEST = $codeProduct[$this->conf->code_produto_cest];
                } else {
                    $productCEST = '';
                }

                if (!empty($codeProduct['special'])) {
                    $productPricePromo = $codeProduct['special'];
                } else {
                    $productPricePromo = '';
                }

                if (!empty($codeProduct['manufacturer'])) {
                    $productBrand = $codeProduct['manufacturer'];
                } else {
                    $productBrand = '';
                }

                if (empty($variation[0])) {
                    $variation = null;
                }
                if (!empty($product['name'])) {
                    $aProducts[] = [
                        'id'          => $product['product_id'],
                        'name'        => $product['name'],
                        'description' => 'b64' . base64_encode($product['description']),
                        'model'       => 'b64' . base64_encode($product['model']),
                        'sku'         => $productSKU,
                        'ncm'         => $productNCM,
                        'cest'        => $productCEST,
                        'brand'       => $productBrand,
                        'pricePromo'  => $productPricePromo,
                        'quantity'    => $product['quantity'],
                        'price'       => $product['price'],
                        'weight'      => $product['weight'],
                        'length'      => $product['length'],
                        'width'       => $product['width'],
                        'height'      => $product['height'],
                        'attribute'   => $product['attribute'],
                        'variation'   => $variation,
                    ];
                }
            }
            $json['success'] = true;
            $json['products'] = $aProducts;
        } else {
            $json['success'] = false;
            $json['error'] = "Problems Getting Products.";
        }

        $this->log->write('products() - Rodado com sucesso');

        if ($this->debugIt) {
            echo '<pre>';
            print_r($json);
            echo '</pre>';
        } else {
            $this->response->setOutput(json_encode($json));
        }
    }

    public function count_products()
    {
        $this->checkPlugin();
        $filters = $this->getParameter();
        $products = $this->model_module_code_bling_native_product->getCountProduct();

        foreach ($products as $product) {
            if ($product['NrProducts'] > 0) {
                $json['success'] = true;
                $json['products'] = $product['NrProducts'];
            } else {
                $json['success'] = false;
                $json['error'] = "Problems in Products Count.";
            }
        }
        if ($this->debugIt) {
            echo '<pre>';
            print_r($json);
            echo '</pre>';
        } else {
            $this->response->setOutput(json_encode($json));
        }
    }

    //Products by FILTERS
    public function products_filters()
    {
        $this->checkPlugin();
        $this->log->write('products_filters() - Rodando');
        $filters = $this->getParameter();
        $products = $this->model_module_code_bling_native_product->getAllProductFilters($filters);

        if (count($products)) {
            foreach ($products as $product) {
                $variation = $this->model_module_code_bling_native_product->getVariation($product);

                if (empty($product['product_id'])) {
                    continue;
                }

                $codeProduct = $this->model_module_code_bling->getProduct($product['product_id']);

                if (empty($codeProduct['product_id'])) {
                    continue;
                }

                $productSKU = '';
                if (!empty($codeProduct['sku'])) {
                    $productSKU = $codeProduct['sku'];
                }

                if (!empty($this->conf->code_produto_ncm) && !empty($codeProduct[$this->conf->code_produto_ncm])) {
                    $productNCM = $codeProduct[$this->conf->code_produto_ncm];
                } else {
                    $productNCM = '';
                }

                if (!empty($this->conf->code_produto_cest) && !empty($codeProduct[$this->conf->code_produto_cest])) {
                    $productCEST = $codeProduct[$this->conf->code_produto_cest];
                } else {
                    $productCEST = '';
                }

                if (!empty($codeProduct['special'])) {
                    $productPricePromo = $codeProduct['special'];
                } else {
                    $productPricePromo = '';
                }

                if (!empty($codeProduct['manufacturer'])) {
                    $productBrand = $codeProduct['manufacturer'];
                } else {
                    $productBrand = '';
                }

                if (empty($variation[0])) {
                    $variation = null;
                }

                $product = $this->model_module_code_bling->correctedProductStoreDescription($product);

                $aProducts[] = [
                    'id'          => $product['product_id'],
                    'name'        => $product['name'],
                    'description' => 'b64' . base64_encode($product['description']),
                    'model'       => 'b64' . base64_encode($product['model']),
                    'sku'         => $productSKU,
                    'ncm'         => $productNCM,
                    'cest'        => $productCEST,
                    'brand'       => $productBrand,
                    'pricePromo'  => $productPricePromo,
                    'quantity'    => $product['quantity'],
                    'price'       => $product['price'],
                    'weight'      => $product['weight'],
                    'length'      => $product['length'],
                    'width'       => $product['width'],
                    'height'      => $product['height'],
                    'attribute'   => $product['attribute'],
                    'variation'   => $variation,
                ];
            }
            $json['success'] = true;
            $json['products'] = $aProducts;
        } else {
            $json['success'] = false;
            $json['error'] = "There are no products on this Period.";
        }

        $this->log->write('products_filters() - Rodado com sucesso');

        if ($this->debugIt) {
            echo '<pre>';
            print_r($json);
            echo '</pre>';
        } else {
            $this->response->setOutput(json_encode($json));
        }
    }

    //insert Products
    public function products_insert()
    {
        $this->checkPlugin();
        $this->log->write('products_insert() - Rodando');

        $parameters = urldecode($this->getParameter());
        $method = 'GET';
        $link = "https://www.bling.com.br/Integrations/Export/class-opencart-export-product.php?auth=" . base64_encode($this->config->get('rest_api_key')) . "&parameters=" . $parameters;

        // create curl resource
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        // $return contains the output string
        $return = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($return);

        //Obter o erro de url
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (empty($return)) {
            $json['success'] = false;
            $json['error'] = "cURL HTTP error " . $code;
            $json['url'] = $link;
        } else {
            //$this->log->write('parametros dados: '.print_r($parameters, true));
            //$this->log->write('result dados: '.print_r($result, true));

            $products = $this->model_module_code_bling_native_product->insert_oc_products($result);

            //this->log->write('products_insert() dados: '.print_r($result, true));

            if (isset($products['id'])) {
                if ($products['returnUp']) {
                    $description = $this->model_module_code_bling->updateProductStore($products['id']);
                    $json['desc'] = $description;

                    if ($description) {
                        $json['idProduto'] = $products['id'];
                        $json['success'] = true;
                    } else {
                        $json['success'] = false;
                        $json['error'] = "Problems Saving Descripton Product. ";
                    }
                } else {
                    $json['success'] = false;
                    $json['error'] = "Problems Updating Product";
                }
            } else {
                foreach ($products as $prod) {
                    $description = $this->model_module_code_bling_native_product->insert_oc_description($result, $prod['maximo']);
                    $this->model_module_code_bling->updateProductStore($prod['maximo']);
                    foreach ($description as $desc) {
                        if ($desc['idMax'] != $prod['maximo']) {
                            $this->model_module_code_bling_native_product->delete_oc_products($prod['maximo']);
                            $json['success'] = false;
                            $json['error'] = "Problems Saving Products.";
                        } else {
                            $json['idProduto'] = $prod['maximo'];
                            $json['success'] = true;
                        }
                    }
                }
            }
        }

        $this->log->write('products_insert() - Rodado com sucesso');

        if ($this->debugIt) {
            echo '<pre>';
            print_r($json);
            echo '</pre>';
        } else {
            $this->response->setOutput(json_encode($json));
        }
    }

    //products_stock
    public function products_stock()
    {
        $this->checkPlugin();

        $parameters = urldecode($this->getParameter());
        $exp = explode("|", $parameters);

        $tipo = $exp[0];
        $id = $exp[1];
        $qtd = $exp[2];

        //$this->log->write('products_stock() dados: '.print_r($exp, true));
        if ($tipo == 'P') {
            $products = $this->model_module_code_bling_native_product->update_stock_product($id, $qtd);
            $this->update_stock_product_by_option('', $id);
        } else {
            $products = $this->model_module_code_bling_native_product->update_stock_variation($id, $qtd);
            $this->update_stock_product_by_option($id, '');
        }

        $json['success'] = $products;

        if ($this->debugIt) {
            echo '<pre>';
            print_r($json);
            echo '</pre>';
        } else {
            $this->response->setOutput(json_encode($json));
        }
    }

    /*
        Codemarket - Melhoria extra para lidar com o bug na Quantidade do Produto Principal quando tem Opções, o Bling retorna quantidade 0
    */
    public function update_stock_product_by_option($product_option_value_id, $product_id)
    {
        if (empty($product_id)) {
            //Verificando o ID do Produto
            $query = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "product_option_value 
                WHERE  
                `product_option_value_id` = '" . $product_option_value_id . "' 
                LIMIT 1
            ");

            if (!empty($query->row['product_id'])) {
                $product_id = $query->row['product_id'];
            }
        }

        $query = $this->db->query("SELECT SUM(quantity) as qtd FROM " . DB_PREFIX . "product_option_value 
            WHERE  
            `product_id` = '" . $product_id . "' 
        ");

        if (isset($query->row['qtd'])) {
            $qtd = $query->row['qtd'];
            $update = $this->db->query("UPDATE `" . DB_PREFIX . "product` 
                SET `quantity`= '" . (int) $qtd . "' 
                WHERE  
                `product_id` = '" . (int) $product_id . "'
            ");
        }
    }

    //products_price
    public function products_price()
    {
        $this->checkPlugin();
        $parameters = urldecode($this->getParameter());
        $exp = explode("|", $parameters);

        $tipo = $exp[0];
        $id = $exp[1];
        $price = $exp[2];

        //$this->log->write('products_price() dados: '.print_r($exp, true));

        if ($tipo == 'P') {
            $products = $this->model_module_code_bling_native_product->update_price_product($id, $price);
        } else {
            $products = $this->model_module_code_bling_native_product->update_price_variation($id, $price);
        }

        $json['success'] = $products;

        if ($this->debugIt) {
            echo '<pre>';
            print_r($json);
            echo '</pre>';
        } else {
            $this->response->setOutput(json_encode($json));
        }
    }

    //All orders
    public function orders()
    {
        $this->checkPlugin();
        $this->log->write('orders() - Rodando');

        /*check offset parameter*/
        if (isset($this->request->get['offset']) && $this->request->get['offset'] != "" && ctype_digit($this->request->get['offset'])) {
            $offset = $this->request->get['offset'];
        } else {
            $offset = 0;
        }

        /*check limit parameter*/
        if (isset($this->request->get['limit']) && $this->request->get['limit'] != "" && ctype_digit($this->request->get['limit'])) {
            $limit = $this->request->get['limit'];
        } else {
            $limit = 10000;
        }

        $filters = '||tds';

        /*get all orders of user*/
        $results = $this->model_module_code_bling_native_order->getAllOrdersFilters($offset, $limit, $filters);

        $orders = [];
        if (count($results)) {
            $orders = $this->ParametersReturnOrder($results);
            $json['success'] = true;
            $json['orders'] = $orders;
        } else {
            $json['success'] = false;
            $json['error'] = "Problems Getting Orders. There are no Orders on this Period.";
        }

        $this->log->write('orders() - Rodado com sucesso');

        if ($this->debugIt) {
            echo '<pre>';
            print_r($json);
            echo '</pre>';
        } else {
            $this->response->setOutput(json_encode($json));
        }
    }

    //Orders by FILTERS
    public function orders_filters()
    {
        $this->checkPlugin();
        $this->log->write('orders_filters() - Rodando');

        $filters = $this->getParameter();
        //$this->log->write('orders_filters() '.print_r($filters,true));

        /*check offset parameter*/
        if (isset($this->request->get['offset']) && $this->request->get['offset'] != "" && ctype_digit($this->request->get['offset'])) {
            $offset = $this->request->get['offset'];
        } else {
            $offset = 0;
        }

        /*check limit parameter*/
        if (isset($this->request->get['limit']) && $this->request->get['limit'] != "" && ctype_digit($this->request->get['limit'])) {
            $limit = $this->request->get['limit'];
        } else {
            $limit = 10000;
        }

        /*get all orders of user*/
        $results = $this->model_module_code_bling_native_order->getAllOrdersFilters($offset, $limit, $filters);

        $orders = [];
        if (count($results)) {
            $orders = $this->ParametersReturnOrder($results);
            $json['success'] = true;
            $json['orders'] = $orders;
        } else {
            $json['success'] = false;
            $json['error'] = "Problems Getting Products.";
        }

        $this->log->write('orders_filters() - Rodado com sucesso');

        if ($this->debugIt) {
            echo '<pre>';
            print_r($json);
            echo '</pre>';
        } else {
            $this->response->setOutput(json_encode($json));
        }
    }

    //Order by ID
    public function order_id()
    {
        $this->checkPlugin();
        $parametro = $this->getParameter();
        //get values of the database
        $results = $this->model_module_code_bling_native_order->getOrderId($parametro);

        $orders = [];
        if (count($results)) {
            $orders = $this->ParametersReturnOrder($results);
            $json['success'] = true;
            $json['orders'] = $orders;
        } else {
            $json['success'] = false;
        }

        if ($this->debugIt) {
            echo '<pre>';
            print_r($json);
            echo '</pre>';
        } else {
            $this->response->setOutput(json_encode($json));
        }
    }

    /**
     *
     * Parameters of the functions
     *
     */

    public function ParametersReturnProduct(array $params = null)
    {
        $products = $params;
        $produtos = [];
        foreach ($products as $product) {
            $variation = $this->model_module_code_bling_native_order->getVariation($product);

            if (empty($product['product_id'])) {
                continue;
            }

            $codeProduct = $this->model_module_code_bling->getProduct($product['product_id']);

            if (empty($codeProduct['product_id'])) {
                continue;
            }

            $productSKU = '';
            if (!empty($codeProduct['sku'])) {
                $productSKU = $codeProduct['sku'];
            }

            if (!empty($this->conf->code_produto_ncm) && !empty($codeProduct[$this->conf->code_produto_ncm])) {
                $productNCM = $codeProduct[$this->conf->code_produto_ncm];
            } else {
                $productNCM = '';
            }

            if (!empty($this->conf->code_produto_cest) && !empty($codeProduct[$this->conf->code_produto_cest])) {
                $productCEST = $codeProduct[$this->conf->code_produto_cest];
            } else {
                $productCEST = '';
            }

            if (!empty($codeProduct['special'])) {
                $productPricePromo = $codeProduct['special'];
            } else {
                $productPricePromo = '';
            }

            if (!empty($codeProduct['manufacturer'])) {
                $productBrand = $codeProduct['manufacturer'];
            } else {
                $productBrand = '';
            }

            if (empty($variation[0])) {
                $variation = null;
            }

            $produtos[] = [
                'order_product_id' => $product['order_product_id'],
                'product_id'       => $product['product_id'],
                'name'             => $product['name'],
                'model'            => $product['model'],
                'sku'              => $productSKU,
                'ncm'              => $productNCM,
                'cest'             => $productCEST,
                'brand'            => $productBrand,
                'pricePromo'       => $productPricePromo,
                'quantity'         => $product['quantity'],
                'price'            => $product['price'],
                'total'            => $product['total'],
                'tax'              => $product['tax'],
                'rewar'            => $product['reward'],
                'variation'        => $variation,
            ];
        }
        return $produtos;
    }

    public function ParametersReturnPayment(array $params = null)
    {
        $result = $params;
        $payment = [
            'payment_name'      => $result['payment_firstname'] . " " . $result['payment_lastname'],
            'payment_company'   => $result['payment_company'],
            'payment_address_1' => $result['payment_address_1'],
            'payment_address_2' => $result['payment_address_2'],
            'payment_city'      => $result['payment_city'],
            'payment_postcode'  => $result['payment_postcode'],
            'payment_country'   => $result['payment_country'],
            'payment_zone'      => $result['payment_zone'],
            'payment_method'    => $result['payment_method'],
            'payment_code'      => $result['payment_code'],
        ];
        return $payment;
    }

    public function ParametersReturnShipping(array $params = null)
    {
        $result = $params;

        $shippingValue = $this->model_module_code_bling_native_order->getShippingByOrder($result['order_id']);
        if (empty($shippingValue[0]['valueShipping']) || $shippingValue[0]['valueShipping'] == null) {
            $shippingValue[0]['valueShipping'] = 0;
        }

        $shipping = [
            'shipping_name'      => $result['shipping_firstname'] . " " . $result['shipping_lastname'],
            'shipping_company'   => $result['shipping_company'],
            'shipping_address_1' => $result['shipping_address_1'],
            'shipping_address_2' => $result['shipping_address_2'],
            'shipping_city'      => $result['shipping_city'],
            'shipping_postcode'  => $result['shipping_postcode'],
            'shipping_country'   => $result['shipping_country'],
            'shipping_zone'      => $result['shipping_zone'],
            'shipping_method'    => $result['shipping_method'],
            'shipping_code'      => $result['shipping_code'],
            'shipping_price'     => $shippingValue[0]['valueShipping'],
        ];
        return $shipping;
    }

    public function ParametersReturnOrder(array $params = null)
    {
        $results = $params;
        foreach ($results as $result) {
            $product_total = $this->model_module_code_bling_native_order->getTotalOrderProductsByOrderId($result['order_id']);
            $voucher_total = $this->model_module_code_bling_native_order->getTotalOrderVouchersByOrderId($result['order_id']);
            $products = $this->model_module_code_bling_native_order->getProductOrderId($result['order_id']);
            $couponDiscount = $this->model_module_code_bling_native_order->getCouponByOrder($result['order_id']);

            //shipping price and/or discount coupon
            if (empty($couponDiscount[0]['valueCoupon']) || $couponDiscount[0]['valueCoupon'] == null) {
                $couponDiscount[0]['valueCoupon'] = 0;
            }

            $productsOrder = $this->ParametersReturnProduct($products);
            $payment = $this->ParametersReturnPayment($result);
            $shipping = $this->ParametersReturnShipping($result);

            //DADOS EXTRAS
            $codeOrder = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order`
                WHERE 
                order_id = '" . (int) $result['order_id'] . "'
                LIMIT 1
            ")->row;

            if (empty($codeOrder['order_id']) || empty($payment)) {
                continue;
            }

            if (version_compare(VERSION, '2.0.3.1', '>')) {
                $customField = json_decode($codeOrder['custom_field'], true);
                $paymentCustomField = json_decode($codeOrder['payment_custom_field'], true);
                $shippingCustomField = json_decode($codeOrder['payment_custom_field'], true);
            } else {
                $customField = unserialize($codeOrder['custom_field']);
                $paymentCustomField = unserialize($codeOrder['payment_custom_field']);
                $shippingCustomField = unserialize($codeOrder['payment_custom_field']);
            }

            $codeCPF = '';
            if (!empty($this->conf->code_extra_cpf) && !empty($customField[$this->conf->code_extra_cpf])) {
                $codeCPF = $customField[$this->conf->code_extra_cpf];
            } else if (!empty($this->conf->code_extra_cpf) && !empty($paymentCustomField[$this->conf->code_extra_cpf])) {
                $codeCPF = $paymentCustomField[$this->conf->code_extra_cpf];
            }

            $codePerson = 'F';
            if (!empty($codeCPF)) {
                $codeCPF = preg_replace("/[^0-9]/", '', $codeCPF);
                $codeCPFL = strlen($codeCPF);

                if ($codeCPFL > 11) {
                    $codePerson = 'J';
                }
            }

            $codePaymentComplement = '';
            if (!empty($this->conf->code_extra_complemento) && !empty($paymentCustomField[$this->conf->code_extra_complemento])) {
                $codePaymentComplement = $paymentCustomField[$this->conf->code_extra_complemento];
            }

            $codePaymentNumber = '';
            if (!empty($this->conf->code_extra_numero) && !empty($paymentCustomField[$this->conf->code_extra_numero])) {
                $codePaymentNumber = $paymentCustomField[$this->conf->code_extra_numero];
            }

            $codePaymentTel = '';
            if (!empty($this->conf->code_extra_celular) && !empty($paymentCustomField[$this->conf->code_extra_celular])) {
                $codePaymentTel = $paymentCustomField[$this->conf->code_extra_celular];
            } else if (!empty($this->conf->code_extra_celular) && !empty($customField[$this->conf->code_extra_celular])) {
                $codePaymentTel = $customField[$this->conf->code_extra_celular];
            }

            $codeShippingComplement = '';
            if (!empty($this->conf->code_extra_complemento) && !empty($shippingCustomField[$this->conf->code_extra_complemento])) {
                $codeShippingComplement = $shippingCustomField[$this->conf->code_extra_complemento];
            }

            $codeShippingNumber = '';
            if (!empty($this->conf->code_extra_numero) && !empty($shippingCustomField[$this->conf->code_extra_numero])) {
                $codeShippingNumber = $shippingCustomField[$this->conf->code_extra_numero];
            }

            $payment['payment_number_address'] = $codePaymentNumber;
            $payment['payment_address_2'] = $codePaymentComplement;
            $payment['payment_neighborhood'] = $codeOrder['payment_address_2'];
            $payment['payment_cellphone'] = $codePaymentTel;

            if (!empty($shipping)) {
                $shipping['shipping_address_2'] = $codeShippingComplement;
                $shipping['shipping_number'] = $codeShippingNumber;
                $shipping['shipping_neighborhood'] = $codeOrder['shipping_address_2'];
            }

            $orders[] = [
                'order_id'        => $result['order_id'],
                'name'            => $result['firstname'] . ' ' . $result['lastname'],
                'customer_id'     => $result['customer_id'],
                'email'           => $result['email'],
                'telephone'       => $result['telephone'],
                'status'          => $result['status'],
                'date_added'      => $result['date_added'],
                'products_totals' => ($product_total + $voucher_total),
                'products'        => $productsOrder,
                'payment'         => $payment,
                'shipping'        => $shipping,
                'discount'        => $couponDiscount[0]['valueCoupon'],
                'total'           => $result['total'],
                'comment'         => $result['comment'],
                'currency_code'   => $result['currency_code'],
                'currency_value'  => $result['currency_value'],
                'cpf_cnpj'        => $codeCPF,
                'persontype'      => $codePerson,

            ];
        }
        return $orders;
    }

    /**
     *
     * get GET parameters
     *
     */
    private function getParameter()
    {
        if (isset($this->request->get['parametro']) && !empty($this->request->get['parametro'])) {
            $parametro = $this->request->get['parametro'];
        } else {
            $parametro = null;
        }

        return $parametro;
    }

    private function checkPlugin()
    {
        $json = ["success" => false];

        /*validate api security key*/
        if ($this->request->get['key'] != $this->secretKey) {
            $this->log->write('checkPlugin() - Chave diferente, chave do Bling: '.$this->request->get['key'].' Chave configurada: '.$this->secretKey);
            $json["error"] = 'Invalid secret key';
        }

        if (isset($json["error"])) {
            $this->response->addHeader('Content-Type: application/json');
            echo(json_encode($json));
            exit;
        } else {
            $this->log->write('checkPlugin() - Verificado com sucesso');
            $this->response->setOutput(json_encode($json));
        }
    }
}

