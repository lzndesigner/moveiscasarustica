<?php

/**
 * © Copyright 2014-2020 Codemarket - Todos os direitos reservados.
 * Métodos auxiliares para adicionar mais recursos ao Bling
 * Class ModelModuleCodeBling
 */
class ModelModuleCodeBling extends Model
{
    private $token;
    private $conf;
    private $urlApi;
    private $status;
    private $log;

    /**
     * ModelModuleCodeBling constructor.
     *
     * @param $registry
     */
    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->load->model('checkout/order');
        $this->load->model('module/codemarket_module');
        $this->load->model('localisation/language');
        $this->load->model('tool/image');

        $conf = $this->model_module_codemarket_module->getModulo('548');
        $this->log = new log('CodeBling.log');

        if (
            empty($conf) || empty($conf->code_habilitar) || (int) $conf->code_habilitar === 2 ||
            empty($conf->code_token) || empty($conf->code_chave)
        ) {
            $this->log->write('Bling desativado, verifique a configuração, conf:' . print_r($conf, true));
            $this->status = false;
            return false;
        }

        $this->status = true;
        $this->conf = $conf;
        $this->token = trim($conf->code_token);
        $this->urlApi = 'https://bling.com.br/Api/v2/';

        return true;
    }

    public function correctedProductStoreDescription($product)
    {
        if (!$this->status) {
            return false;
        }

        if (empty($product['product_id'])) {
            return $product;
        }

        //$this->log->write('correctedProductStoreDescription() Produto ' . print_r($product, true));

        $product_id = $product['product_id'];
        $query = $this->getProduct($product_id);

        //$this->log->write('correctedProductStoreDescription() SQL ' . $sql);
        $this->log->write('correctedProductStoreDescription() Produto SQL ' . print_r($query, true));

        if (!empty($query['name'])) {
            $product['name'] = trim($query['name']);
        }

        if (!empty($query['description'])) {
            $product['description'] = trim($query['description']);
        }

        $this->updateProdutoBling($query);

        $this->log->write('correctedProductStoreDescription() Array Final ' . print_r($product, true));
        return $product;
    }

    public function getProduct($product_id)
    {
        $language_id = $this->languageId();
        $query = $this->db->query("SELECT DISTINCT *, pd.name AS name, p.image, m.name AS manufacturer, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special, (SELECT points FROM " . DB_PREFIX . "product_reward pr WHERE pr.product_id = p.product_id AND pr.customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "') AS reward, (SELECT ss.name FROM " . DB_PREFIX . "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" . (int) $language_id . "') AS stock_status, (SELECT wcd.unit FROM " . DB_PREFIX . "weight_class_description wcd WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = '" . (int) $language_id . "') AS weight_class, (SELECT lcd.unit FROM " . DB_PREFIX . "length_class_description lcd WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = '" . (int) $language_id . "') AS length_class, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2 WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id) AS reviews, p.sort_order FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) WHERE p.product_id = '" . (int) $product_id . "' AND pd.language_id = '" . (int) $language_id . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int) 0 . "' LIMIT 1");

        return $query->row;
    }

    public function getProductBling($codigo)
    {
        if (!$this->status) {
            return false;
        }

        $url = 'https://bling.com.br/Api/v2/produto/' . $codigo . '/json&apikey=' . $this->token . '&imagem=S';
        //echo $url;
        $getProduct = $this->get($url);

        return $getProduct;
    }

    public function updateProdutoBling($product)
    {
        //$this->log->write('updateProdutoBling() 1');
        if (!$this->status || empty($product['image']) || empty($product['sku'])) {
            return false;
        }

        $url = 'https://bling.com.br/Api/v2/produto/' . $product['sku'] . '/json/';
        $linkProduct = $this->url->link('product/product', '&product_id=' . $product['product_id']);

        $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
        $xml .= '  <produto>';
        $xml .= '  <codigo>' . $product['sku'] . '</codigo>';
        $xml .= '  <linkExterno>' . $linkProduct . '</linkExterno>';

        if (!empty($this->conf->code_image_exportar) && $this->conf->code_image_exportar == 1) {
            $linkImg = $this->config->get('config_ssl') . 'image/' . $product['image'];

            $xml .= '  <imagens>';
            $xml .= '  <url>' . $linkImg . '</url>';

            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image 
                WHERE product_id = '" . (int) $product['product_id'] . "'
                ORDER BY sort_order ASC
            ");

            if (!empty($query->rows[0]['image'])) {
                foreach ($query->rows as $img) {
                    if (!empty($img['image'])) {
                        $linkImg = $this->config->get('config_ssl') . 'image/' . $img['image'];
                        $xml .= '  <url>' . $linkImg . '</url>';
                    }
                }
            }

            $xml .= '  </imagens>';
        } else {
            $this->log->write('Configurado para não exportar imagens');
        }

        $xml .= '  </produto>';

        $data = [
            'apikey' => $this->token,
            'xml'    => rawurlencode($xml),
        ];

        //$this->log->write('updateProdutoBling() Data ' . print_r($data, true));
        $updateProduct = $this->post($data, $url);

        return true;
    }

    /**
     *
     * Alterando os Produtos com novos novos dados, como: model novo, imagens, meta titulo, seo_url...
     *
     * @param $product_id
     *
     * @return bool
     */
    public function updateProductStore($product_id)
    {
        if (!$this->status) {
            return false;
        }

        $this->log->write('updateProductStore() product_id: ' . $product_id);

        $query = $this->db->query("SELECT sku FROM " . DB_PREFIX . "product WHERE product_id = '" . (int) $product_id . "' LIMIT 1");

        if (empty($query->row['sku'])) {
            return false;
        }

        $codigo = $query->row['sku'];
        $getProduct = $this->getProductBling($codigo);
        //$this->log->write('Produto Bling Return: ' . print_r($getProduct, true));

        if (empty($getProduct['retorno']['produtos'][0]['produto']['codigo'])) {
            return false;
        }

        $product = $getProduct['retorno']['produtos'][0]['produto'];
        $this->updateProdutoStoreSQL($product, $product_id);
        $this->saveImageStore($product['descricao'], $product['imagem'], $product_id);

        $this->log->write('updateProductStore() Rodado com sucesso');

        return true;
    }

    /**
     * Retorna o ID da linguagem pt-br
     *
     * @return mixed
     */
    private function languageId()
    {
        $languages = $this->model_localisation_language->getLanguages();

        foreach ($languages as $l) {
            if (empty($l['code']) || $l['code'] != 'pt-br' || empty($l['language_id'])) {
                continue;
            }

            return $l['language_id'];
        }
    }

    /**
     *
     * Salvando no banco as alterações do Produto
     *
     * @param $product
     * @param $product_id
     *
     * @return bool
     */
    private function updateProdutoStoreSQL($product, $product_id)
    {
        $this->log->write('updateProdutoStoreSQL() product_id: ' . $product_id);

        $ean = trim($product['gtin']);
        $descriptionHTML = trim($product['descricaoCurta']);
        $title = trim($product['descricao']);
        $slug = $this->slug($title);
        $model = trim($product['codigo']);

        $model = substr($model, 0, 64);

        $this->db->query("UPDATE " . DB_PREFIX . "product SET 
            model = '" . $this->db->escape($model) . "',
            ean =   '" . $this->db->escape($ean) . "'
            WHERE product_id = '" . (int) $product_id . "'
        ");

        $this->log->write('updateProdutoStoreSQL() Atualizado Produto');
        /*
         * $this->db->query("UPDATE " . DB_PREFIX . "product SET
            model = '" . $this->db->escape($model) . "',
            ean =   '" . $this->db->escape($ean) . "',
            weight =   '" . $this->db->escape(number_format($product['pesoBruto'], 2, '.', '')) . "',
            length =   '" . $this->db->escape(number_format($product['profundidadeProduto'], 2, '.', '')) . "',
            width =   '" . $this->db->escape(number_format($product['larguraProduto'], 2, '.', '')) . "',
            height =   '" . $this->db->escape(number_format($product['alturaProduto'], 2, '.', '')) . "'
            WHERE product_id = '" . (int) $product_id . "'
        ");
         */

        $language_id = $this->languageId();

        //Título, Descrição em HTML e Meta Título
        $this->db->query("UPDATE " . DB_PREFIX . "product_description SET
            name = '" . $this->db->escape($title) . "', 
            description = '" . $this->db->escape($descriptionHTML) . "',
            meta_title =   '" . $this->db->escape($title) . "'
            WHERE product_id = '" . (int) $product_id . "' AND
            language_id = '" . (int) $language_id . "'       
        ");

        //SEO URL
        if (version_compare(VERSION, '3.0.0.0', '>=')) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'product_id=" . (int) $product_id . "' AND language_id =  '" . (int) $language_id . "'");
            $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url 
               SET 
               store_id = 0, 
               language_id = '" . (int) $language_id . "', 
               query = 'product_id=" . (int) $product_id . "', 
               keyword = '" . $this->db->escape($slug) . "'
           ");
        } else {
            $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int) $product_id . "'");
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias 
               SET 
               query = 'product_id=" . (int) $product_id . "', 
               keyword = '" . $this->db->escape($slug) . "'
           ");
        }

        //Salvando na Loja 0 o Produto
        $query = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int) $product_id . "' AND store_id = 0 LIMIT 1");
        if (empty($query->row['product_id'])) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int) $product_id . "', store_id = 0");
        }

        $this->log->write('updateProdutoStoreSQL() Atualizado descrição');

        return true;
    }

    /**
     *
     * Cada imagem é salva novamente nos arquivos e alterada no produto
     * Na tabela product_image é removida as imagens do produto e salvo novamente as referências
     * Leva 1-2s por imagem salva, ideal evitar mais de 30 imagens
     *
     * @param $title
     * @param $images
     * @param $product_id
     *
     * @return bool
     */
    private function saveImageStore($title, $images, $product_id)
    {
        if (empty($title) || empty($images)) {
            return false;
        }

        if (!empty($this->conf->code_image_importar) && $this->conf->code_image_importar == 2) {
            $this->log->write('saveImageStore() Configurado para não importar imagens');
            return false;
        }

        $title = $this->slug($title);

        $folder = 'catalog/bling/';
        @mkdir(DIR_IMAGE . $folder, 0755);
        $path = DIR_IMAGE . $folder;

        $image_types_allowed = [
            'image/gif'  => '.gif',
            'image/jpeg' => '.jpg',
            'image/png'  => '.png',
            'image/webp' => '.webp',
        ];

        foreach ($images as $key => $img) {
            if (empty($img['link'])) {
                continue;
            }

            $url = $img['link'];
            $extension = '';

            //$type = image_type_to_mime_type( exif_imagetype( $url ) );
            $image_info = getimagesize($url);
            //print_r($image_info);

            /*
            if (empty($image_info['mime']) || !in_array($image_info['mime'], $image_types_allowed)) {
                continue;
            }*/

            foreach ($image_types_allowed as $type => $ext) {
                if ($type == $image_info['mime']) {
                    $extension = $ext;
                }
            }

            if (empty($extension)) {
                continue;
            }

            $imgDownload = $this->getImage($url);

            if (empty($imgDownload['image'])) {
                continue;
            }

            file_put_contents($path . $title . $key . $extension, $imgDownload['image']);
            $filePath = $folder . $title . $key . $extension;

            if ($key == 0) {
                //Primeira imagem, usar como capa

                $this->db->query("
                UPDATE " . DB_PREFIX . "product SET
                image = '" . $this->db->escape($filePath) . "'
                WHERE product_id = '" . (int) $product_id . "'"
                );
            } else {
                /**
                 * Remover a tabela das imagens
                 * Inserir no banco
                 */

                if ($key == 1) {
                    $this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int) $product_id . "'");
                }

                $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET 
                    product_id = '" . (int) $product_id . "', 
                    image = '" . $this->db->escape($filePath) . "', 
                    sort_order = '" . (int) $key . "'
                ");
            }
        }

        $this->log->write('saveImageStore() Atualizado imagens');

        return true;
    }

    /**
     * AUXILIARES SLUG, POST E GET
     */

    /** Cria o Slug
     *
     * @param $str
     * @param array $replace
     * @param string $delimiter
     *
     * @return string $slug
     */
    private function slug($str, $replace = [], $delimiter = '-')
    {
        $table = [
            'Š' => 'S', 'š' => 's', 'Đ' => 'Dj', 'đ' => 'dj', 'Ž' => 'Z', 'ž' => 'z', 'Č' => 'C', 'č' => 'c', 'Ć' => 'C', 'ć' => 'c',
            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E',
            'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O',
            'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss',
            'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e',
            'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o',
            'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'ý' => 'y', 'þ' => 'b',
            'ÿ' => 'y', 'Ŕ' => 'R', 'ŕ' => 'r',
        ];

        $str = strtr($str, $table);

        //Script criado por chluehr https://gist.github.com/chluehr/1632883
        if (!empty($replace)) {
            $str = str_replace((array) $replace, ' ', $str);
        }
        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

        return $clean;
        //Fim do script por chluehr
    }

    /**
     * @param $data array
     * @param $url string
     *
     * @return bool|mixed
     */
    private function post($data, $url)
    {
        if (empty($data)) {
            return false;
        }

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => 'UTF-8',
            CURLOPT_TIMEOUT        => 80,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "POST",
            CURLOPT_POST           => count($data),
            CURLOPT_POSTFIELDS     => $data,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER     => [
                "Cache-Control: no-cache",
                //"Content-Type: application/json",
                // "token: " . $this->conf->token,
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            $this->log->write('Error Curl' . print_r($err, true));
        } else {
            $this->log->write('Return ' . print_r($response, true));

            return json_decode($response, true);
        }
    }

    /**
     * @param $url string
     *
     * @return bool|mixed
     */
    private function get($url)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => 'UTF-8',
            CURLOPT_MAXREDIRS      => 3,
            CURLOPT_TIMEOUT        => 12,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER     => [
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                //"token: " . $this->conf->token,
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            $this->log->write('Error Curl' . print_r($err, true));
        } else {
            //$this->log->write('Dados retornados' . print_r(json_decode($response, true), true));
            return json_decode($response, true);
        }
    }

    /**
     * @param $url string
     *
     * @return bool|mixed
     */
    private function getImage($url)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS      => 3,
            CURLOPT_TIMEOUT        => 12,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER     => [
                "Cache-Control: no-cache",
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            $this->log->write('Error Curl' . print_r($err, true));
        } else {
            return ['image' => $response];
        }
    }
}
