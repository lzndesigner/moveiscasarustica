<?php
// Heading
$_['express_text_title']      = 'Confirmação de Pagamento';

// Text
$_['text_title']              = '<b>PayPal</b> - Cartão de Crédito e Saldo em Conta';
$_['text_cart']               = 'Carrinho de compras';
$_['text_shipping_updated']   = 'Serviço de frete atualizado';
$_['text_trial']              = '%s cada %s %s para %s pagamentos ';
$_['text_recurring']          = '%s cada %s %s';
$_['text_recurring_item']     = 'Item Recorrente';
$_['text_length']             = ' para %s pagamentos';

// Entry
$_['express_entry_coupon']    = 'Insira seu cupom aqui:';

// Button
$_['button_express_coupon']   = 'Adicionar';
$_['button_express_confirm']  = 'Confirmar';
$_['button_express_login']    = 'Continuar para o Pagamento';
$_['button_express_shipping'] = 'Atualizar frete';

// Error
$_['error_heading_title']	  = 'Havia um erro';
$_['error_too_many_failures'] = 'Seu pagamento falhou muitas vezes';
$_['error_unavailable'] 	  = 'Use a finalização da compra completa com este pedido';
$_['error_no_shipping']    	  = 'Aviso: Não há opções de envio disponíveis. Por favor <a href="%s">entre em contato</a>';
